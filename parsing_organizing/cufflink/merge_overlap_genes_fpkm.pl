#!/usr/bin/perl
#Created by wade
#function: merge duplicated/overlapped genes in cufflink output file. merge reference_contig, sum fpkm, find lowest min fpkm & find highest max fpkm.
#Input: cufflink fpkm file
#Output: duplicate gene_ID merged
#Example: 


use strict;
use Data::Dumper;

my $infile=shift;
my $outfile=shift;

open IN,"<$infile" or die "can not open $infile";
open OUT,">$outfile" or die "can not open $outfile";


my $header=<IN>;
print OUT $header;


my %data;
while (<IN>) {
    chomp;

    
    my @record= split "\t",$_;
    my $record_ref= \@record;
    my $gene_id=$record[3];

    if ( $data{$gene_id} ){
	
	push @{$data{$gene_id}},$record_ref;
#	print Dumper($data{$gene_id});
    }else{
	my @new_record=($record_ref);
	$data{$gene_id}=\@new_record;
	
    }
    

}



for my $gene_id (keys(%data)){

    if (@{$data{$gene_id}} >1){
	my @merged_data=@{$data{$gene_id}[0]};

	my $maxfpkm=str2float($data{$gene_id}[0][11]);
	my $minfpkm=str2float($data{$gene_id}[0][10]);
	my $fpkm=0;
	my $reference="";

# compare all duplicate data and merge them
	foreach my $i (@{$data{$gene_id}}) {
	    if(str2float(@$i[11]) > $maxfpkm) {$maxfpkm=str2float(@$i[11])};
	    if(str2float(@$i[10]) < $minfpkm) {$minfpkm=str2float(@$i[10])};
	    $fpkm=$fpkm+str2float(@$i[9]);
	    $reference=@$i[6].'&'.$reference;

	}

	@merged_data[6]=$reference;
	@merged_data[9]=$fpkm;
	@merged_data[10]=$minfpkm;
	@merged_data[11]=$maxfpkm;

	my @new_data=(\@merged_data);
	$data{$gene_id}=\@new_data;
       

    }
    
}



for my $gene_id (keys(%data)){

    my $output=join("\t",@{@{$data{$gene_id}}[0]});
    print OUT $output."\n";

}



sub str2float{
    my $string=shift;
    my $float=sprintf("%.5f",$string);
    return $float;
}
