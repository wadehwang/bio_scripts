#!/pkg/biology/python/python-2.7.6/bin/python
import os
import sys
import time
from optparse import OptionParser
from threading import Thread
import multiprocessing as mp
import numpy as np

#from operator import itemgetter, attrgetter
#import ast
parser = OptionParser()
parser.add_option("-f", "--file", dest="filename",help="input file")
parser.add_option("-t", "--processes", dest="processes",help="number of processes")
parser.add_option("-c", "--column", dest="column",help="column of categories")
parser.add_option("-a", "--allmatch", dest="allmatch",help="output file containing  all matched query only")
parser.add_option("-p", "--partialmatch", dest="partialmatch",help="output file contain partially matched queury only")
parser.add_option("-m", "--partialmatchmask", dest="partialmatchmask",help="output file contain partially matched queury mask only")
(options, args) = parser.parse_args()

 

class Slice_data:
    def __init__(self,file,slice_number,column):
        self.column=column
        self.FILE=open(file,'Ur')
        filesize=os.stat(file).st_size
        print "filesize= "+str(filesize)+" bytes"
   #self.FILE.seek(0,0)
#        self.slicesize=filesize//slice_number+1
        self.slicesize=filesize
        if  self.slicesize < 2000000:
            self.slicesize= 2000000
        self.thisslice=[]
        self.nextslice=[]
        self.nonext=0

    def __iter__(self):
        return self
    def next(self):
        #if no next slice is TRUE then stop iteration
        print self.nonext
        if self.nonext==1: 
            raise StopIteration
        
        newslice=self.FILE.readlines(self.slicesize)
        #print "number of line in new slice:"+str(len(newslice))
        if len(newslice)==0: 
            self.nonext=1
        
        self.nextslice.extend(newslice)
        self.thisslice=self.nextslice
        
        
        sliceoffset=0
        print "number of line in this slice:"+str(len(self.thisslice))
        while True:
            #if  two lines are in different categories than split them
            if len(self.thisslice)==1:
                break
            elif self.thisslice[-(sliceoffset+1)].split("\t")[self.column-1]!=self.thisslice[-(sliceoffset+2)].split("\t")[self.column-1]:
                self.nextslice=self.thisslice[len(self.thisslice)-(sliceoffset+1):]
                self.thisslice=self.thisslice[:len(self.thisslice)-(sliceoffset+1)]
                break
            else:
                #else search backward by one line
                sliceoffset +=1
                #if all slice are in the same categories
                if len(self.thisslice) == sliceoffset+1:
                    # if there is no next slice then break
                    if self.nonext==1: 
        
                        break
                    else:
                        #get nextslice
                        newslice=self.FILE.readlines(self.slicesize)
                        print "number of line in new slice:"+str(len(newslice))
                        #if there is no next slice set the nonext to TRUE
                        if len(newslice)==0:
                            self.nonext=1
                        #add next slice to current slice                        
                        self.thisslice.extend(newslice)
                        
                        sliceoffset=0
        print self.nonext
        return self


#give chunks by categories in given column. The list have to be sorted by that column first.
class Chunks:
    chunkNum=0
    category=""
    def __init__(self,slice,size,column):
        
        self.chunksize=size
        self.catecol=int(column)
        self.slice=slice

    
        self.thisline=""

        self.lastline=""
        self.thischunk=""
        self.nonext=0
        self.next()
    def __iter__(self):
        return self

    def next(self):
        
        if  self.nonext==1: raise StopIteration
        self.thischunk=self.thisline
        while True:
#            print "%%%%%"
#            print self.thischunk
#            print "%%%%%%"
            self.lastline=self.thisline
            try:
                self.thisline=self.slice.pop(0).strip()
            except:
                self.nonext=1
                return self
            
            
            
            this_cate=self.thisline.split('\t')[self.catecol-1].strip()
            last_cate=self.lastline.split('\t')[self.catecol-1].strip()
                          
            if this_cate!=last_cate:
                break

            self.thischunk=self.thischunk+'\n'+self.thisline
        

        return self

#Blast data of same query
class Blast_Data:
    def __init__(self,blastdata):
        self.data=blastdata
        self.matrix=[]
        for line in self.data.split('\n'):
#            print line
            vector=[]

            for item in line.strip().split('\t'):
                try: 
                    
                    vector.append(toNum(item.strip()))
                except ValueError:
                    vector.append(item.strip())
                    
            self.matrix.append(vector)
        
#Blast data with same query and with query length 13th column
class Blast_Data_with_QLen(Blast_Data):
    
    def partialmatch(self):
        partialmatched=[]
        for vector in  self.matrix:
            if vector[3]!=vector[12]:
                partialmatched.append(vector)
        return partialmatched  
    def totalmatch(self):
        totalmatched=[]
        for vector in  self.matrix:
            if vector[3]==vector[12]:
                totalmatched.append(vector)
        if totalmatched:
            return totalmatched
    
def toNum(s):
    s=s.strip()
    try:
        return int(s)
    except ValueError:
        return float(s)
                 
def covered_mask(partialmatched_data):
    #the query length is in column 13th
    mask=np.zeros(partialmatched_data[0][12])
    for vector in partialmatched_data:
        for i in range(vector[6],vector[7]):            
            mask[i-1]=1
    return "".join(mask.astype(int).astype(str))

def print_matrix(array_in_array):
#convert matrix to tab delimited output string
    output=[]
    for vector in array_in_array:
        output.append("\t".join(map(str,vector)))
    return "\n".join(output)+"\n"

def categorize_chunk(slice,allmatch,partialmatch,partialmatchmask, allmatch_lock, partmatch_lock, mask_lock):
    
    chunks = Chunks(slice,"",options.column)
    
    ALLMATCH=open(allmatch,'w')
    PARTMATCH=open(partialmatch,'w')
    PARTMATCHMASK=open(partialmatchmask,'w')
    
    for chunk in chunks:
        
        blast_data=Blast_Data_with_QLen(chunk.thischunk)
        totalmatch=blast_data.totalmatch()
        partialmatch=blast_data.partialmatch()

        if totalmatch:
            print "totalmatch"
            totalmatch_sorted=sorted(totalmatch, key=lambda x: -x[2])
            print print_matrix(totalmatch_sorted)
            allmatch_lock.acquire()
            ALLMATCH.write(print_matrix( totalmatch_sorted ))
            allmatch_lock.release()
        else:
            #if len(partialmatch)<2:
                #print "partialmatched partialunique"
                #print print_matrix(partialmatch)
                #partmatch_lock.acquire()
                #PARTMATCH.write(print_matrix( partialmatch))
                #partmatch_lock.release()
            #else:
            
            print "partialmatched fragmented"
                
            partialmatch_sorted=sorted(blast_data.partialmatch(), key=lambda x: -x[3])
            mask_lock.acquire()
            PARTMATCHMASK.write( str(partialmatch_sorted[0][0])+"\t"+covered_mask(partialmatch_sorted)+"\n" )
            mask_lock.release()
            print  print_matrix(partialmatch_sorted)
            partmatch_lock.acquire()
            PARTMATCH.write(print_matrix(partialmatch_sorted))
            partmatch_lock.release()	
	#writter
#write oject that prints out the results in older
##def writter():

  ###  )
    
if __name__ == '__main__':

    slices = Slice_data(options.filename,int(options.processes),1)
    print  options.processes
    pool=mp.Pool(processes= int(options.processes))

    allmatch_lock=mp.Lock()
    partmatch_lock=mp.Lock()
    mask_lock=mp.Lock()
    #result=[pool.apply_async(categorize_chunk, args=(slice.thisslice,options.allmatch, options.partialmatch, options.partialmatchmask, lock)) for slice in slices]

    procs= [mp.Process(target=categorize_chunk, args=(slice.thisslice,options.allmatch, options.partialmatch, options.partialmatchmask, allmatch_lock, partmatch_lock, mask_lock)) for slice in slices]

    for p in procs: p.start()
    for p in procs: p.join()
