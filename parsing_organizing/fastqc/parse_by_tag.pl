#!/usr/bin/perl

# 20150420 by Wade
#usage : perl file parse_by_tag.pl begin_tag end_tag
#example: perl parse_by_tag.pl ../../../YM_008_1_2P.fq_fastqc/fastqc_data.txt '>>Basic Statistics' '>>END_MODULE'


use strict;


my ( $file, $tag_begin, $tag_end)=@ARGV;
if (!$tag_begin ||!$tag_end){ die "Please provide start tag and end tag\n" ;}

open IN,"<$file" or die "can not open $file";

while(<IN>) {
   
   
     if (/^$tag_begin/../^$tag_end/) {
	 print $file."\t".$_ unless /$tag_begin/ or /$tag_end/ or /^\s*$/;
     }

   
}



