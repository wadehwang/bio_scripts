#!/usr/bin/perl
# usge: 
#      cat table.txt| perl data_to_pivot.pl colnumber1 colnumber2 colnumber3
# where
#      colnumber1: number of column that will be the row in pivot
#      colnumber2: number of column that will be the column in pivot
#      colnumber3: number of column that will be the value in pivot
#Example:
# datasets:
# A       1       1800
# A       2       3678
# A       3       4736
# A       4       2342
# B       1       2342
# B       2       4532
# B       3       2321
# C       1       5643
# C       3       2345
#
# command: perl data_to_pivot.pl test.txt 1 2 3
# output pivot:
#            1       2       3       4
#    A       1800    3678    4736    2342
#    B       2342    4532    2321
#    C       5643            2345 



use strict;
use Data::Dumper;
use List::Uniq;


#my $filename=shift;
my $pivot_row_no=shift;
my $pivot_colum_no=shift;
my $pivot_value_no=shift;

#open IN,"-" or die "can not read input";



my %hash={};
my $hash_ref=\%hash;
my @all_key1;
my @all_key2;
while (<STDIN>){
    chomp;
    my @line=split("\t",$_);
    
    my $key1=$line[$pivot_row_no-1];
    my $key2=$line[$pivot_colum_no-1];
    my $value=$line[$pivot_value_no-1];

    $hash_ref->{$key1}->{$key2} = $value;


    unless ($key1 ~~ @all_key1) {push (@all_key1 , $key1);}
    unless ($key2 ~~ @all_key2) {push (@all_key2 , $key2);}

}

my @sorted_key1= sort { $a cmp $b } @all_key1;
my @sorted_key2= sort { $a cmp $b } @all_key2;
#print Dumper \%hash;

#print header
print "\t";
foreach my $key2  (@sorted_key2){
    my @key2_array=split(" ",$key2);
    my $joined_key2= join("_",@key2_array);
    print $joined_key2."\t";
}

#print contenet
foreach my $key1 (@sorted_key1){
    
    print "\n";
    print $key1."\t";
    foreach my $key2 (@sorted_key2){
	
	print $hash_ref->{$key1}->{$key2}."\t";
    
    }



}





