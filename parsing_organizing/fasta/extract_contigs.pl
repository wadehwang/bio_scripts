#!/usr/bin/perl
#extra fasta record for categoriztion result
use Bio::SeqIO;
use strict;


sub main {

    my ($contig_fasta_in, $allmatch_fasta_out, $partialmatch_fasta_out, $nomatch_fasta_out, $allmatch_in, $partialmatch_in)= @ARGV;
    
    my $inseq=Bio::SeqIO->new(
	-file => "<$contig_fasta_in",
	-format => "fasta" ,
	);
    
    my $allmatch_out=Bio::SeqIO->new(
	-file => ">$allmatch_fasta_out",
	-format => "fasta" ,
	);

    my $partmatch_out=Bio::SeqIO->new(
	-file => ">$partialmatch_fasta_out",
	-format => "fasta" ,
	);
    
    my $nomatch_out=Bio::SeqIO->new(
	-file => ">$nomatch_fasta_out",
	-format => "fasta" ,
	);
    

    my $allmatch_hash = creat_hash($allmatch_in);
    my $partialmatch_hash = creat_hash($partialmatch_in);

    while (my $seq = $inseq->next_seq) {
    
	if ($allmatch_hash->{$seq->id}){
	
	    $allmatch_out->write_seq($seq);
	    
	} elsif ($partialmatch_hash->{$seq->id}){
	    
	    $partmatch_out->write_seq($seq);
    
	}else{
	    
	    $nomatch_out->write_seq($seq);
	    
	    
	}
    }
    
}
    
sub creat_hash {
    my ($file)=@_;
    open IN,"<$file" or die "can not open $file ";
    my %name_hash;
    while (<IN>){
	chomp;
	my $id=(split("\t",$_ ))[0];
	$name_hash{$id}=1;
	
    }
    close IN;
    return \%name_hash;
    
}

main()
