#!/usr/bin/perl
####
# extract sequeces by a list of ID
#usage: perl extract_seq_by_IDlist.pl All_sequences List_of_id Sequence_in_IDlist
#example :perl extract_contigs.pl all_contigs.fa list_of_ID contigs_in_IDlist.fa
####
use Bio::SeqIO;
use strict;


sub main{
    my($fasta_in, $list_in, $fasta_out)=@ARGV;
    
    my $inseq=Bio::SeqIO->new(
        -file => "<$fasta_in",
        -format => "fasta" ,
	);
    
    my $outseq=Bio::SeqIO->new(
        -file => ">$fasta_out",
        -format => "fasta" ,
	);
    
    my $list_hash = creat_hash($list_in);
    
    while (my $seq = $inseq->next_seq) {
	
	if ($list_hash->{$seq->id}){
	    
	    $outseq->write_seq($seq);
	    
	}
	
    }
    
}
sub creat_hash {
    my ($file)=@_;
    open IN,"<$file" or die "can not open $file ";
    my %name_hash;
    while (<IN>){
	chomp;
	my $id=(split("\t",$_ ))[0];
	$name_hash{$id}=1;
	
    }
    close IN;
    return \%name_hash;
    
}

main()
