#!/usr/bin/perl
use Data::Dumper;
use strict;
use LWP::Simple;
use List::MoreUtils qw(uniq);
my $file=shift;
#my ($s1,$s2,$s3,$s4)=@ARGV;


#open S1,"<$s1" or die "can not open $s1";
#open S2,"<$s2" or die "can not open $s2";
#open S3,"<$s3" or die "can not open $s3";
#open S4,"<$s4" or die "can not open $s4";

open FILE,"<$file" or die "can not open $file";

my %data;

#my $header=<FILE>;
my %speciess;
while (<FILE>){

    chomp;
    my $trancript=(split "\t",$_)[1];
    my $fpkm=(split "\t",$_)[18];
    my $species=(split "\t",$_)[0];
    $speciess{$species}=1;
#    print Dumper %data;
    if ($data{$trancript}){
	if ($data{$trancript}->{"FPKM"} != $fpkm){
	    print "Warning: $species $trancript has conflict fpkm";
	    exit;
	}else{
	    
	    $data{$trancript}->{"FPKM"}=$fpkm ;
	}
	
	if ($data{$trancript}->{$species}){
	    my @new_data=split("\t",$_);
	    push  $data{$trancript}->{$species}, \@new_data;

	}else{
	    my @new_data=split("\t",$_);
	    my @new_array=(\@new_data);
	    my %new_hash;
	    
	    $data{$trancript}{$species}=\@new_array;
	}
    }else{
	my %new_hash;
	my @new_data=split("\t",$_);
	my @new_array=(\@new_data);
	$data{$trancript}=\%new_hash;
	$data{$trancript}->{"FPKM"}=$fpkm;
	$data{$trancript}{$species}=\@new_array;
    }




}



close FILE;

#print Dumper(%data);
my @transcripts=keys(%data);
my @transcripts_sortedby_fpkm=sort{$data{$b}{"FPKM"} <=> $data{$a}{"FPKM"}  } @transcripts;

#printf "transcript\tFPKM\t%s\n", join("\t",keys(%speciess));
print "transcript\tFPKM\tS1------\tS2-------\tS3------\tS4------\tEnzyme\tFunction\n";
foreach my $transcript (@transcripts_sortedby_fpkm){
    print $transcript."\t".$data{$transcript}{"FPKM"};
    my @enzymes;
    my @pathways;
    foreach my $species (keys(%speciess)){
	#print Dumper($data{$transcript}{$species}) ;
	if ($data{$transcript}{$species}){
	    @{$data{$transcript}{$species}}= sort{ ${$b}[3] <=> ${$a}[3]  } @{$data{$transcript}{$species}};
	#print Dumper($data{$transcript}{$species}) ;
	}
	$data{$transcript}{$species}=$data{$transcript}{$species}[0];
#	print Dumper($data{$transcript}{$species}) ;
	my $uniprotID=$data{$transcript}{$species}[2];
	my $ecnumber=$data{$transcript}{$species}[17];
	if($ecnumber){
	    my $output=join('|', ($uniprotID,$ecnumber));
	    push @enzymes,find_enzyme_by_ec($ecnumber);
	    push @pathways,find_pathway_by_ec($ecnumber);
	    print "\t".$output ;
	}else{
	    print "\t".$uniprotID;
	}
    }
    @enzymes=uniq @enzymes;
    @pathways=uniq @pathways;
    my $enzymes=join('|',@enzymes);
    my $pathways=join('|',@pathways);
    print "\t".$enzymes."\t".$pathways;
    print "\n";
}




#print Dumper(%data);
#print Dumper %data;


sub find_enzyme_by_ec{

    my $query=shift;
    my $url = "http://rest.kegg.jp/find/ec/$query/";
    my $content = get($url);
    $content =~ s/%20/ /g;
    


    if($content =~ /ec:$query\s+(.+)\n/ ){
	
	return $query.'=>'."\{$1\}";
    }else{
	return "no enzyme found";
    }

}


sub find_pathway_by_ec{

    my $query=shift;

    my $url = "http://rest.kegg.jp/link/pathway/$query";
    my $pathways = get($url);
    $pathways =~ s/%20/ /g;
    my @pathways_annot;
#print $pathways;
    while( $pathways =~ /(ec\:[\.0-9\-]+)\s+path\:(\w+)\n/g){

	if($1 && $2){
#           print $2;
	    my $ecnumber=$1;
	    my $pathway=$2;
	    my $url = "http://rest.kegg.jp/get/$2/";
	    my $content = get($url);
	    $content =~ s/%20/ /g;
#           print $content;
            if( $content =~ /NAME\s+(.+)\n/){
#                print "$ecnumber\t$pathway\t$1\n";
                push @pathways_annot, $ecnumber.'=>'."\{$1\}";
            }
	}
    }

    @pathways_annot=uniq @pathways_annot;
    chomp(@pathways_annot);
    my $pathways_annot=join ";",@pathways_annot;

    return $pathways_annot;

 
}
