#!/usr/bin/perl

use Storable;
my $hashfile=shift;
my $queryfile=shift;
my $outputfile=shift;
open IN, "<$queryfile" or die "can not open $queryfile";
open OUT, ">$outputfile" or die "can not open $outputfile";
my $datahash=retrieve($hashfile);

my $output="";

while (<IN>){
    chomp;
    my $id= (split '\|' , (split "\t", $_)[1] )[2];
    print OUT $_."\t".$datahash->{$id}."\n";
    
}
close OUT;
close IN;
