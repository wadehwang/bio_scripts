#!/usr/bin/perl

use strict;
use Storable;
my $sprot_dat=shift;
my $trembl_dat=shift;
my $datahash_file=shift;

#open IN,"<$uniprot_dat" or die "can not open $uniprot_dat";
open SP,"zcat $sprot_dat|" or die "can not open $sprot_dat";
open TR,"zcat $trembl_dat|" or die "can not open $trembl_dat";

my %datahash={};
my $datahash=\%datahash;

$/="\n//\n";



while(<SP>){
    chomp;

    my $id= (grep{/^ID/} split("\n",$_))[0];
    $id=((split " ", $id))[1];

    my $ec= (map {/EC=([0-9\-]+.[0-9\-]+.[0-9\-]+.[0-9\-])/ ? $1 :()} split("\n",$_))[0];


    $datahash->{$id}=$ec;

    print $id."==>".$ec."\n";

}

while(<TR>){
    chomp;

    my $id= (grep{/^ID/} split("\n",$_))[0];
    $id=((split " ", $id))[1];

    my $ec= (map {/EC=([0-9\-]+.[0-9\-]+.[0-9\-]+.[0-9\-])/ ? $1 :()} split("\n",$_))[0];


    $datahash->{$id}=$ec;
    
    print $id."==>".$ec."\n";

}

store $datahash, $datahash_file;

close SP;
close TR;
