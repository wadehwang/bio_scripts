#!/usr/bin/sh

function _extract_trimmed_position(){
    awk '$2!=$3{print $0}' $1 > $1.trimmed
}

export -f _extract_trimmed_position



