#!/usr/bin/sh

function fastq_length_histagram(){

awk 'NR%4==2{print length($0)}' $1 |sort -n |uniq -c

}

export -f fastq_length_histagram


