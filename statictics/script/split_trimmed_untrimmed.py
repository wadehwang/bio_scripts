#!/usr/bin/python

###This script will separate untrimmed and trimmed sequence by comparing to sequences  before trimmed
###for example, File A had been trimmed according to certain criteria,  and the trimmed/untrimmed sequence are save to file B.
###This program will separate trimmed/untrimmed seqeunce in file B. And script will also extract the seqeunce before trimmed from A.
import os
from Bio import SeqIO
from optparse import OptionParser
import sys
import re

parser=OptionParser()
parser.add_option("-u","--untrimmedfile" , type="string" , dest="untrimmed", help="untrimmed(original) file")
parser.add_option("-t","--trimmedfile" , type="string" , dest="trimmed", help="trimmed file(contain trimmed and untrimmed seqeunce)")
parser.add_option("-1", "--trimmedonly", type="string", dest="trimmedonly", help="ouput filename for trimmed sequences only ")
parser.add_option("-2", "--untrimmedonly", type="string", dest="untrimmedonly", help="ouput filename for untrimmed sequences only")
parser.add_option("-3", "--trimmedoriginal", type="string", dest="trimmedorg", help="original sequences of trimmed sequence ")


(options, args)=parser.parse_args()

UNTRIMMEDONLY=open(options.untrimmedonly, "w")
TRIMMEDONLY=open(options.trimmedonly, "w")
TRIMMED=open(options.trimmed, "rU")
TRIMMEDORG==open(options.trimmedorg, "w")
untrimmed_record_dict=SeqIO.index(options.untrimmed,"fastq")

for trimmed_record in SeqIO.parse(TRIMMED,"fastq"):
     if trimmed_record.id in untrimmed_record_dict:
         untrimmed_record=untrimmed_record_dict[trimmed_record.id]
         if len(untrimmed_record.seq)==len(trimmed_record.seq):
              SeqIO.write(trimmed_record,UNTRIMMEDONLY,"fastq")
         elif len(untrimmed_record.seq)>len(trimmed_record.seq):
              SeqIO.write(trimmed_record,TRIMMEDONLY,"fastq")
              SeqIO.write(untrimmed_record,TRIMMEDORG,"fastq")
         else: 
              sys.exit("error trimmed longer than untrimmed:" + trimmed_record.id) 
         
     else:
         sys.exit("error not exist:" + trimmed_record.id)




TRIMMED.close()
UNTRIMMEDONLY.close()
TRIMMEDONLY.close()
TRIMMEDORG.close()
