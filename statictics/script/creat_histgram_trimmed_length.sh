#!/usr/bin/sh
function _create_histgram(){

    awk 'NR>1{print $2-$3}' $1 |sort -n |uniq -c > $1.LenOfTrimmedSeq_hist
}

export -f _create_histgram

ls *_trimpostion.txt.trimmed | xargs -I{} bash -c "bsub -q 4G -e ./log/%J_{}_createhist.err -o ./log/%J_{}_createhist.out _create_histgram {}"