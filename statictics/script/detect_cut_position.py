#!/usr/bin/python
import os
from Bio import SeqIO
from optparse import OptionParser
import sys
import re

parser=OptionParser()
parser.add_option("-u","--untrimmedfile" , type="string" , dest="untrimmed", help="untrimmed file")
parser.add_option("-t","--trimmedfile" , type="string" , dest="trimmed", help="trimmed file")
parser.add_option("-o", "--output", type="string", dest="output", help="ouput file")
(options, args)=parser.parse_args()

try:
     sys.stdout =open(options.output,"w")
except:
     pass

UNTRIMMED=open(options.untrimmed, "rU")
trimmed_record_dict=SeqIO.index(options.trimmed,"fastq")
print "ID\tUntrimmed_length\tTrimmed_length\tstart_positions\tend_position"
for untrimmed_record in SeqIO.parse(UNTRIMMED,"fastq"):
     if untrimmed_record.id in trimmed_record_dict:
         trimmed_record=trimmed_record_dict[untrimmed_record.id]

         try:
              start = untrimmed_record.seq.find(trimmed_record.seq)
         except:
              sys.exit("Sequence can not match from trimmed sequeunce to untrimmed sequence for "+untrimmed_recod.id)
         
         end = start + len(trimmed_record.seq)-1
         
         print untrimmed_record.id+"\t"+str(len(untrimmed_record.seq))+"\t"+str(len(trimmed_record.seq))+"\t"+str(start)+"\t"+str(end)

     else:
         print untrimmed_record.id+"\t"+"not_exist"




UNTRIMMED.close()
