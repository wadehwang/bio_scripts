#!/usr/bin/sh

zcat $1|awk -v name=$2 'BEGIN{base=0;read=0}{if(NR%4==2){base+=length($0);read+=1;}}END{print name"\t"base"\t"read;}' 